def test_passwd_file(host):
    passwd = host.file("/etc/passwd")
    assert passwd.contains("root")
    assert passwd.user == "root"
    assert passwd.group == "root"
    assert passwd.mode == 0o644


def test_ansible_is_installed(host):
    ansible = host.package("ansible")
    assert ansible.is_installed
    assert ansible.version.startswith("2.9")


def test_ansible_running_and_enabled(host):
    ansible = host.service("ansible")
    assert ansible.is_running
    assert ansible.is_enabled
