# ansible runner in a container

This runner will run the ansible-playbook instead of the cli mode.

We are going to use the built-in container registry within the gitlab project.
Authentication will be done with a [Personal Access Token](https://gitlab.com/help/user/profile/personal_access_tokens) instead of a password.

```bash
$ docker login registry.gitlab.com
...

Login Succeeded
```

## Image build

Now we can add an image to this registry with the following commands:

```bash
# Alpine
$ docker build \
    --tag registry.gitlab.com/besmirzanaj/ansible-systems:alpine \
    --build-arg GIT_COMMIT=$(git rev-parse -q --verify HEAD) \
    --build-arg BUILD_DATE=$(date -u +"%Y-%m-%dT%H:%M:%SZ") \
    -f runners/Dockerfile.alpine .
$ docker push registry.gitlab.com/besmirzanaj/ansible-systems:alpine
```

```bash
# Ubuntu
$ docker build \
    --tag registry.gitlab.com/besmirzanaj/ansible-systems:ubuntu \
    --build-arg GIT_COMMIT=$(git rev-parse -q --verify HEAD) \
    --build-arg BUILD_DATE=$(date -u +"%Y-%m-%dT%H:%M:%SZ") \
    -f runners/Dockerfile.ubuntu .
$ docker push registry.gitlab.com/besmirzanaj/ansible-systems:ubuntu
```

## Registry credentials in k8s

Adding the docker credentials is achieved as per [docs](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/).

Since we already did `docker login` in the previous steps we can assume that the `docker.json` file exists already.

```bash
[[ -f ~/.docker/config.json ]] &&  echo true || echo false
true
```

Now we ca create a new registry credential for our cluster so that it can pull the images in case we need to run some tests

```bash
$ kubectl create secret generic regcred --from-file=.dockerconfigjson=~/.docker/config.json --type=kubernetes.io/dockerconfigjson
secret/regcred created
```

## Linting  

Use hadolint for `Dockerfile` linting. From [here](https://github.com/hadolint/hadolint).

```bash
docker run --rm -i hadolint/hadolint < ./runners/Dockerfile.alpine
docker run --rm -i hadolint/hadolint < runners/Dockerfile.ubuntu
# or from Google registry
docker run --rm -i ghcr.io/hadolint/hadolint < ./runners/Dockerfile.alpine
docker run --rm -i ghcr.io/hadolint/hadolint < ./runners/Dockerfile.ubuntu
```

### Docker image labels
We can check the docker image labels with

```bash
docker inspect registry.gitlab.com/besmirzanaj/ansible-systems:alpine | jq -r '.[0].Config.Labels'
# or
docker inspect registry.gitlab.com/besmirzanaj/ansible-systems:ubuntu | jq -r '.[0].Config.Labels'
```