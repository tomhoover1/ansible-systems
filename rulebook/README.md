# Ansible Rulebook - Event Driven

## Set up - install

From here - https://ansible-rulebook.readthedocs.io/en/stable/installation.html

sudo apt-get --assume-yes install openjdk-17-jdk python3-pip
export JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64
export PATH=$PATH:~/.local/bin
pip3 install ansible ansible-rulebook ansible-runner

### set locales
export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
sudo dpkg-reconfigure locales


## Getting started

https://ansible-rulebook.readthedocs.io/en/stable/getting_started.html
