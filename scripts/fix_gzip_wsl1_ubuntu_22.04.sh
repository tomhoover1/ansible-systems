#!/bin/bash

# https://github.com/microsoft/vscode-remote-release/issues/6704
echo -en '\x10' | sudo dd of=/usr/bin/gzip count=1 bs=1 conv=notrunc seek=$((0x189))
