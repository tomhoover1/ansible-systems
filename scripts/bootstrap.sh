#!/bin/bash

# Bootstrap the remote hosts to accept ansible commands
# designed for CentOS/RedHat systems

# Install recommended python packages/libraries
ansible -i inventory/vps.ini all -u root -m raw -a "yum install -y python libselinux-python"

# Install Ansible stuff on the runner (on on the docker container)
ansible -i inventory/vps.ini all -u root -m raw -a "yum install -y epel-release"
ansible -i inventory/vps.ini all -u root -m raw -a "yum install -y yamllint ansible ansible-yaml"

# Add the ssh keys for root user (using root user here)
ansible -i inventory/vps.ini all -u root -m raw -a "mkdir -p /root/.ssh/"
ansible -i inventory/vps.ini all -u root -m raw -a "curl https://github.com/besmirzanaj.keys -o /root/.ssh/authorized_keys"

# create deployer user and ssh keys for ansible runner
ansible -i inventory/vps.ini all -u root -m raw -a 'useradd deployer'
ansible -i inventory/vps.ini all -u root -m raw -a 'mkdir -p /home/deployer/.ssh/'
ansible -i inventory/vps.ini all -u root -m raw -a 'curl http://ks.cloudalbania.com/gitlab-runner-id_rsa.pub -o /home/deployer/.ssh/authorized_keys'
ansible -i inventory/vps.ini all -u root -m raw -a 'chown deployer /home/deployer/.ssh/authorized_keys'
ansible -i inventory/vps.ini all -u root -m raw -a 'chmod 0600 /home/deployer/.ssh/authorized_keys'
ansible -i inventory/vps.ini all -u root -m raw -a 'usermod -aG sudo deployer'
ansible -i inventory/vps.ini all -u root -m raw -a 'echo "deployer ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/deployer'

# prepare gitlab runner machine to use docker
ansible-galaxy collection install -r ./requirements.yml --force
ansible-galaxy role install -r ./requirements.yml --force
ansible-playbook -u root -i inventory/vps.ini playbooks/gitlab-runner.yml