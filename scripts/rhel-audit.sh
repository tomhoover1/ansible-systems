#!/bin/nash

for i in $serverlist do

connection_host=ssh $i hostname
cpu_count=$(cat /proc/cpuinfo 2>/dev/null | grep '^processor\s*.' | wc -l)
cpu_socket_count=$(cat /proc/cpuinfo 2>/dev/null | grep 'physical id' | sort -u | wc -l)
date_anaconda_log=$(ls --full-time /root/anaconda-ks.cfg 2>/dev/null | grep -o '[0]\{4\}-[0-9]\{2\}-[0-9]\{2\}')
date_machine_id=$(ls --full-time /etc/machine-id 2>/dev/null | grep -o '[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}')
date_yum_history=$(yum history 2>/dev/null | tail -n 2 | grep -o '[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}')
dmi_system_product_name=$(/usr/sbin/dmidecode -s bios-version 2>/dev/null)
dmi_system_uuid=$(/usr/sbin/dmidecode -s system-uuid 2>/dev/null)
etc_release_release=$(cat /etc/*-release | grep PRETTY_NAME=)
redhat_packages_certs=$(ls /etc/pki/product/ /etc/pki/product-default/ 2> /dev/null |grep '.pem' | sort -u | tr '\n' ';')
redhat_packages_gpg_last_built=$(rpm -qa --qf "%{BUILDTIME} %{DSAHEADER:pgpsig}|%{RSAHEADER:pgpsig}|%{SIGGPG:pgpsig}|%{SIGPGP:pgpsig} |%{NAME}-%{VERSION} Built:%{BUILDTIME:date}\n" | grep 'Key ID 199e2f91fd431d51\|Key ID 5326810137017186\|Key ID 45689c882fa658e0\|Key ID 219180cddb42a60e\|Key ID 7514f77d8366b0d9\|Key ID 45689c882fa658e0' | sort -nr | head -n 1 | cut -d"|" -f2)
redhat_packages_gpg_last_installed=$(rpm -qa --qf "%{INSTALLTIME} %{DSAHEADER:pgpsig}|%{RSAHEADER:pgpsig}|%{SIGGPG:pgpsig}|%{SIGPGP:pgpsig} |%{NAME}-%{VERSION} Installed:%{INSTALLTIME:date}\n" | grep 'Key ID 199e2f91fd431d51\|Key ID 5326810137017186\|Key ID 45689c882fa658e0\|Key ID 219180cddb42a60e\|Key ID 7514f77d8366b0d9\|Key ID 45689c882fa658e0' | sort -nr | head -n 1 | cut -d"|" -f2)
redhat_packages_gpg_num_installed_packages=$(rpm -qa | wc -l)
redhat_packages_gpg_num_rh_packages=$(rpm -qa --qf "%{DSAHEADER:pgpsig}|%{RSAHEADER:pgpsig}|%{SIGGPG:pgpsig}|%{SIGPGP:pgpsig}\n" 2> /dev/null | grep 'Key ID 199e2f91fd431d51\|Key ID 5326810137017186\|Key ID 45689c882fa658e0\|Key ID 219180cddb42a60e\|Key ID 7514f77d8366b0d9\|Key ID 45689c882fa658e0' | wc -l)
subman_consumed=$(subscription-manager list --consumed 2>/dev/null | grep -e '^SKU' -e '^Subscription Name' | sed -n -e 's/SKU\s*.\s*//p' -e 's/Subscription Name\s*.\s*//p')
uname_hostname=$(uname -n)
uname_kernel=$(uname -r)
uname_os=$(uname -s)

#echo "CSV output"
#echo "connection_host,cpu_count,cpu_socket_count,date_anaconda_log,date_machine_id,date_yum_history,dmi_system_product_name,dmi_system_uuid,etc_release_release,redhat_packages_certs,redhat_packages_gpg_last_built,redhat_packages_gpg_last_installed,redhat_packages_gpg_num_installed_packages,redhat_packages_gpg_num_rh_packages,subman_consumed,uname_hostname,uname_kernel,uname_os"

echo $connection_host,$cpu_count, $cpu_socket_count, $date_anaconda_log, $date_machine_id, $date_yum_history, $dmi_system_product_name, $dmi_system_uuid, $etc_release_release, $redhat_packages_certs, $redhat_packages_gpg_last_built, $redhat_packages_gpg_last_installed, $redhat_packages_gpg_num_installed_packages, $redhat_packages_gpg_num_rh_packages, $subman_consumed, $uname_hostname, $uname_kernel, $uname_os

done

