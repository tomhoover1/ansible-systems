# Some notes

## MetalLB
To allow metallb in firewall, run this:

```bash
ansible -i inventory/ubuntu.ini ubuntu_k8s2 -u ubuntu -b -m raw -a 'ufw allow 7964/tcp'
```

## Patch ubuntu boxes

    ansible-playbook -i inventory/ubuntu -u ubuntu -b playbooks/ubuntu-patch.yml

## Patch proxmox nodes
You need to use a specific python version, e.g. `ansible_python_interpreter=/usr/bin/python3` and also make sure to install the following:
    ansible -i inventory/proxmox all -u root -m shell -a 'apt install -y python3-apt, needrestart'
    ansible -i inventory/proxmox all -u root -m shell -a 'echo "pve-kernel" > /var/run/reboot-required.pkgs'

then run the playbook

    ansible-playbook -i inventory/proxmox -u root -b playbooks/ubuntu-patch.yml