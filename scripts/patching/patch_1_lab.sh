#!/bin/bash
PATCH_PLAYBOOK="playbooks/os-patch.yml"
PATCH_INVENTORY="inventory/lab/hosts.ini"
ansible-playbook -u root -i $PATCH_INVENTORY $PATCH_PLAYBOOK -l !proxmox -v
