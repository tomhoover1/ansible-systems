#!/bin/bash
PATCH_PLAYBOOK="playbooks/os-patch.yml"
PATCH_INVENTORY="inventory/ubuntu/ubuntu.ini"
ansible-playbook -u ubuntu -b -i $PATCH_INVENTORY $PATCH_PLAYBOOK -v
