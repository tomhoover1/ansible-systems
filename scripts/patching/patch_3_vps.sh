#!/bin/bash
PATCH_PLAYBOOK="playbooks/os-patch.yml"
PATCH_INVENTORY="inventory/vps.ini"
ansible-playbook -u root -i $PATCH_INVENTORY $PATCH_PLAYBOOK -v
