#!/bin/bash

time for i in $(ls scripts/proxmox_images | grep prepare_); do 
  LINE="=====================BUILDING $i========================"
  echo $LINE
  time ./scripts/proxmox_images/$i
done
