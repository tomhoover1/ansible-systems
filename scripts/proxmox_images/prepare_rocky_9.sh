#!/bin/bash

# prepare_rocky_9.sh

set -eux pipefail

# Declare variables
image="Rocky-9-GenericCloud.latest.x86_64.qcow2"
image_url="https://dl.rockylinux.org/pub/rocky/9/images/x86_64/Rocky-9-GenericCloud.latest.x86_64.qcow2"
vmname="rocky-9-cloud-img-template"
storage_location="wd_ssd_1tb"

# Set the VM ID
if [ "$1" = "" ]; then
  echo "Enter the vmid for this image. Leave empty for default one (5003)"
  read vmid
else
  vmid='5004'
fi

# Get a fresh image
rm -f $image
wget $image_url

# Customize the cloud image
export LIBGUESTFS_BACKEND=direct
virt-customize -a $image -smp 4 --run-command 'sed -i s/^SELINUX=.*$/SELINUX=permissive/ /etc/selinux/config'
virt-customize -a $image -smp 4 --run-command 'echo "fastestmirror=1" >> /etc/dnf/dnf.conf'
virt-customize -a $image -smp 4 --install 'vim,bash-completion,wget,curl,qemu-guest-agent'
virt-customize -a $image -smp 4 --run-command 'yum update -y && yum clean all'

# preparing image to be cloned
virt-sysprep -a $image --colours

# clean up existing template
qm show $vmid >> /dev/null 2>&1 && qm destroy $vmid

# create a VM to import the new image
qm create $vmid --memory 2048 --net0 virtio,bridge=vmbr1 --name $vmname

# import the previously prepared image on this VM
qm importdisk $vmid $image $storage_location

# customize the VM
qm set $vmid --scsihw virtio-scsi-pci --scsi0 $storage_location:vm-$vmid-disk-0,aio=native
qm set $vmid --ide2 $storage_location:cloudinit
qm set $vmid --boot c --bootdisk scsi0
qm set $vmid --agent enabled=1
# Fixing this kernel error:
# fatal glibc error: cpu does not support x86-64-v2
qm set $vmid --cpu cputype=host

# convert the vm to a template
qm template $vmid

# cleanup
rm -f $image