#!/bin/bash

# NOTE
# cloud-init needs a static IP address. 
# the VM will no work with DHCP 
# https://access.redhat.com/discussions/2213791

# prepare_centos_7.sh

set -eux pipefail

# Declare variables
image="CentOS-7-x86_64-GenericCloud.qcow2"
image_url="https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud.qcow2"
vmname="centos-7-cloud-img-template"
storage_location="wd_ssd_1tb"

# Set the VM ID
if [ "$1" = "" ]; then
  echo "Enter the vmid for this image. Leave empty for default one (5003)"
  read vmid
else
  vmid='5010'
fi

# Get a fresh image
rm -f $image
wget $image_url

# Customize the cloud image
export LIBGUESTFS_BACKEND=direct
virt-customize -a $image -smp 4 --run-command 'sed -i s/^SELINUX=.*$/SELINUX=permissive/ /etc/selinux/config'
virt-customize -a $image -smp 4 --install 'vim,bash-completion,wget,curl,qemu-guest-agent,libguestfs-tools'
virt-customize -a $image -smp 4 --run-command 'yum update -y && yum clean all'

# preparing image to be cloned
virt-sysprep -a $image --colours

# clean up existing template
qm show $vmid >> /dev/null 2>&1 && qm destroy $vmid

# create a VM to import the new image
qm create $vmid --memory 2048 --net0 virtio,bridge=vmbr1 --name $vmname

# import the previously prepared image on this VM
qm importdisk $vmid $image $storage_location

# customize the VM
qm set $vmid --scsihw virtio-scsi-pci --scsi0 $storage_location:vm-$vmid-disk-0,aio=native
qm set $vmid --ide2 $storage_location:cloudinit
qm set $vmid --boot c --bootdisk scsi0
qm set $vmid --agent enabled=1

# convert the vm to a template
qm template $vmid

# cleanup
rm -f $image