#!/bin/bash

# prepare_ubuntu_2204.sh

set -eux pipefail

# Declare variables
image="jammy-server-cloudimg-amd64.img"
image_url="https://cloud-images.ubuntu.com/jammy/current"
vmname="ubuntu-2204-cloud-img-template"
storage_location="wd_ssd_1tb"

# Set the VM ID
if [ "$1" = "" ]; then
  echo "Enter the vmid for this image. Leave empty for default one (5001)"
  read vmid
else
  vmid='5001'
fi

# Get a fresh image
rm -f $image
wget $image_url/$image

# Customize the cloud image
virt-customize -a $image -smp 4 --install qemu-guest-agent

# preparing image to be cloned
virt-sysprep -a $image --colours

# clean up existing template
qm show $vmid >> /dev/null 2>&1 && qm destroy $vmid

# create a VM to import the new image
qm create $vmid --memory 2048 --net0 virtio,bridge=vmbr1 --name $vmname --bootdisk scsi0

# import the previously prepared image on this VM
qm importdisk $vmid $image $storage_location

# customize the VM
qm set $vmid --scsihw virtio-scsi-pci --scsi0 $storage_location:vm-$vmid-disk-0,aio=native
qm set $vmid --ide2 $storage_location:cloudinit
qm set $vmid --boot c --bootdisk scsi0
qm set $vmid --agent enabled=1

# convert the vm to a template
qm template $vmid

# cleanup temporary downloaded cloud image
rm -f $image