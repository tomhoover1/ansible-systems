# Syncthing on synology DS211

The latest Syncthing version available from [Synocommunity](https://synocommunity.com/package/syncthing) will install a non compatible binary with the DS211 CPU. The binary will show this in the cosole log when running the binary manually:

```bash
$ /volume1/@appstore/syncthing# ./bin/syncthing
[TVAWV] 2022/12/03 11:50:22 INFO: Access the GUI via the following URL: http://127.0.0.1:8384/
[TVAWV] 2022/12/03 11:50:22 WARNING: GUI/API: accept tcp 127.0.0.1:8384: accept: function not implemented (restarting)
[TVAWV] 2022/12/03 11:50:22 INFO: My name is "DiskStation"
[TVAWV] 2022/12/03 11:50:22 WARNING: Syncthing should not run as a privileged or system user. Please consider using a normal user account.
[TVAWV] 2022/12/03 11:50:22 INFO: GUI and API listening on 127.0.0.1:8384
[TVAWV] 2022/12/03 11:50:22 INFO: Access the GUI via the following URL: http://127.0.0.1:8384/
[TVAWV] 2022/12/03 11:50:22 WARNING: GUI/API: accept tcp 127.0.0.1:8384: accept: function not implemented (restarting)
[TVAWV] 2022/12/03 11:50:22 INFO: GUI and API listening on 127.0.0.1:8384
[TVAWV] 2022/12/03 11:50:22 INFO: Access the GUI via the following URL: http://127.0.0.1:8384/
[TVAWV] 2022/12/03 11:50:22 WARNING: GUI/API: accept tcp 127.0.0.1:8384: accept: function not implemented (restarting)
```

A [workaround}(https://github.com/syncthing/syncthing/issues/8325#issuecomment-1213882377) is to use a lower version, 1.19.2, so that it is compatible with the runinng kernel.

So we get the binary with the specified version from ther link above for the `6.1 88f628x` cpu architecture at this [link](https://packages.synocommunity.com/syncthing/25/syncthing.v25.f15047%5B88f628x%5D.spk) and then install the downloaded `.spk` file manually from the Package Center.

To make the app start and not about the configuration file version being newer than the binaries:

```bash
[start] 12:05:39 INFO: syncthing v1.19.0 "Fermium Flea" (go1.17.5 linux-arm) root@publish 2022-02-03 19:51:15 UTC
[start] 12:05:40 WARNING: Failed to initialize config: config file version (37) is newer than supported version (35). If this is expected, use --allow-newer-config to override.
[monitor] 12:05:40 INFO: Syncthing exited: exit status 1
```

then we configure the config file located at the var/options.conf of syncthing installation like follows:

```bash
cat /volume1/@appstore/syncthing/var/options.conf
#!/bin/sh
# This file will be added to start-stop-status script
# For possible options see syncthing --help

# Example: uncomment this to start syncthing with all devices paused
SYNCTHING_OPTIONS="--no-upgrade  --allow-newer-config"
```

Finally start the app from the Package Center and continue using synthing as usual. Don't forget to set permissions for the sc-syncthing user on the folder to be shared and also read permissins on the parent folder so that it can tranverse the directories.

