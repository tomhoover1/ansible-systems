[![pipeline status](https://gitlab.com/besmirzanaj/ansible-systems/badges/master/pipeline.svg)](https://gitlab.com/besmirzanaj/ansible-systems/-/commits/master)

# Building a simple infrastructure deploying configuration using Ansible Playbooks

-------------------------------------------

This repo requires Ansible 2.8+.

These repo is meant to be a reference and starter's guide to building Ansible
Playbooks. This playbook was tested on CentOS 7 so I recommend that you use
CentOS or RHEL to test this repo.

The purpose of this repo will change over time but basically will contain some
common roles and test playbooks for configuration management.
We are also configuring a gitlab_runner host running in a CLI or dockerized setup.

Two nodes are recommended as a minimunm with the gitlab_runner role ideally in a
separate host.

## Folder structure

```bash
playbook-folder $ tree -L 2
.
├── ansible.cfg
├── inventory
│   ├── group_vars
│   ├── hosts
│   └── hosts_vars
├── LICENSE.md
├── playbooks
│   ├── common.yml
│   ├── logs.yml
│   ├── os-patch.yml
│   ├── site.yml
│   └── web.yml
├── README.md
├── requirements.yml
├── roles
│   ├── besmirzanaj.ansible_resolv_conf
...
│   └── web
├── scripts
│   ├── bootstrap.sh
│   ├── install-ansible-termux.sh
│   └── manual_ansible_update
├── vars
│   ├── apache.yml
│   ├── gitlab_runner.yml
│   ├── logzio.yml
│   └── main.yml
└── vault.yml
```

## Sample inventory file

```ini
[webservers]
vps1.cloudalbania.com

[dbservers]
vps2.cloudalbania

[gitlab-runner]
vps3.cloudalbania.com
```

## How to work with this repo

### Ansible installation

Install ansible in your workstation according to <a rel="instructions" href="
https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html">these instructions</a>.

### SSH keys

Use your own method of generating a key and push that key in the authorized_keys for user root (note, not secure)

```bash
host$ ssh-copy-id root@destination.ip.com
```

In the destination box

```bash
destination.ip.com$ cat /root/.ssh/authorized_keys
```

Or just run some of the bootstrap commands in `scripts/bootstrap.sh`.

The best implementation would be to integrate the SSH key during host creation.
If it is a cloud provider most of them ask for the key to inject duing host
creation. If the host is created within your organization, you can push the key
during the kickstart/terraform process.

### Running the playbook

The stack can be deployed using the following command afterwards:

```bash
ansible-galaxy collection install -r ./requirements.yml --force
ansible-galaxy role install -r ./requirements.yml --force
ansible-playbook -u root -i inventory/production.ini playbooks/gitlab-runner.yml
```

### Checking the playbook run

Once done, you can check the results at the [pipeline results]( https://gitlab.com/besmirzanaj/ansible-systems/pipelines).

The project is integrated with a gitlab runner ans scheduled every hour to make sure the infrastructure is kept up-to-date.

## Copyright (C) 2023 Besmir Zanaj (besmirzanaj@gmail.com)

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
